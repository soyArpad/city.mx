//  Copyright © 2016 Arpad Larrinaga Aviles
//
//  City.mx is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  City.mx is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with City.mx.  If not, see <http://www.gnu.org/licenses/>.

import UIKit

struct Config {
    struct General {
        struct Color {
            static let main = UIColor.whiteColor()
            static let secundary = UIColor.blueColor()
        }
    }
    struct BlueMix {
        static let route = "http://citymx.mybluemix.net"
        static let guid = "13c2c32c-2316-4375-9108-8c4c9ac83f4a"
    }
    
    struct Color {
        struct Main {
            static let background = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
            //static let primary = UIColor(red: 100/255, green: 181/255, blue: 246/255, alpha: 1)
            //static let primary = UIColor(red: 56/255, green: 0/255, blue: 151/255, alpha: 1)
            static let primary = UIColor(red: 228/255, green: 46/255, blue: 147/255, alpha: 1)
            static let secundary = UIColor.whiteColor()
        }
        struct Text {
            static let title = UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 1)
            static let highlight = UIColor(red: 100/255, green: 181/255, blue: 246/255, alpha: 1)
        }
    }
}