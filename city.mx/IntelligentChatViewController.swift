//  Copyright © 2016 Arpad Larrinaga Aviles
//
//  City.mx is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  City.mx is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with City.mx.  If not, see <http://www.gnu.org/licenses/>.

import UIKit
import Chatto
import ChattoAdditions
import Alamofire

class IntelligentChatViewController: ChatViewController {

    var messageSender: FakeMessageSender!
    var dataSource: FakeDataSource! {
        didSet {
            self.chatDataSource = self.dataSource
        }
    }
    
    lazy private var baseMessageHandler: BaseMessageHandler = {
        return BaseMessageHandler(messageSender: self.messageSender)
    }()
    
    override func viewDidLoad() {
        title = "City MX"
        dataSource = FakeDataSource(count: 0, pageSize: 50)
        messageSender = dataSource.messageSender
        addIncomingMessage("Hola, en que te puedo ayudar?")
        super.viewDidLoad()
        super.chatItemsDecorator = ChatItemsDemoDecorator()
    }
    
    @objc
    private func addRandomIncomingMessage() {
        self.dataSource.addRandomIncomingMessage()
    }
    
    func addIncomingMessage(message: String) {
        self.dataSource.addIncomingMessage(message)
    }
    
    var chatInputPresenter: ChatInputBarPresenter!
    override func createChatInputView() -> UIView {
        let chatInputView = ChatInputBar.loadNib()
        self.configureChatInputBar(chatInputView)
        self.chatInputPresenter = ChatInputBarPresenter(chatInputView: chatInputView, chatInputItems: self.createChatInputItems())
        return chatInputView
    }
    
    func configureChatInputBar(chatInputBar: ChatInputBar) {
        var appearance = ChatInputBarAppearance()
        appearance.sendButtonTitle = NSLocalizedString("Send", comment: "")
        appearance.textPlaceholder = NSLocalizedString("Type a message", comment: "")
        chatInputBar.setAppearance(appearance)
    }
    
    override func createPresenterBuilders() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        return [
            TextMessageModel.chatItemType: [
                TextMessagePresenterBuilder(
                    viewModelBuilder: TextMessageViewModelDefaultBuilder(),
                    interactionHandler: TextMessageHandler(baseHandler: self.baseMessageHandler)
                )
            ],
//            PhotoMessageModel.chatItemType: [
//                PhotoMessagePresenterBuilder(
//                    viewModelBuilder: FakePhotoMessageViewModelBuilder(),
//                    interactionHandler: PhotoMessageHandler(baseHandler: self.baseMessageHandler)
//                )
//            ],
            SendingStatusModel.chatItemType: [SendingStatusPresenterBuilder()]
        ]
    }
    
    func createChatInputItems() -> [ChatInputItemProtocol] {
        var items = [ChatInputItemProtocol]()
        items.append(self.createTextInputItem())
        //items.append(self.createPhotoInputItem())
        return items
    }
    
    private func createTextInputItem() -> TextChatInputItem {
        let item = TextChatInputItem()
        item.textInputHandler = { [weak self] text in
            self?.dataSource.addTextMessage(text)
            self?.askQuestion(question: text)
        }
        return item
    }
    
    private func createPhotoInputItem() -> PhotosChatInputItem {
        let item = PhotosChatInputItem(presentingController: self)
        item.photoInputHandler = { [weak self] image in
            self?.dataSource.addPhotoMessage(image)
        }
        return item
    }
    
    // MARK: Server Calls
    
    func askQuestion(question text: String) {
        let parameters = [
            "text": text
        ]
        
        Alamofire.request(.POST, "http://192.168.1.123:3040/watson/classify", parameters: parameters, encoding: .JSON).responseJSON { response in
            switch response.result {
            case .Success(let JSON):
                let resp = JSON as! NSDictionary
                let success = resp["success"] as! Int
                if success == 1 {
                    self.addIncomingMessage(resp["message"] as! String)
                }
                else  {
                    
                }
                // Post Notification to update Main UI
            case .Failure(let error):
                print(error)
            }
        }
    }

}
