//  Copyright © 2016 Arpad Larrinaga Aviles
//
//  City.mx is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  City.mx is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with City.mx.  If not, see <http://www.gnu.org/licenses/>.

import UIKit
import PureLayout

class WalktroughViewController: UIViewController {

    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var enterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enterButton.layer.masksToBounds = true
        enterButton.layer.cornerRadius = 8
        
        navigationController?.navigationBarHidden = true
        
        configureBackgrounds()
    }
    
    func configureBackgrounds() {
        var previousImageView: UIImageView? = nil
        for index in 0..<3 {
            if index == 0 {
                let imageView = UIImageView(forAutoLayout: ())
                imageView.image = UIImage(named: "walkthrough1")
                imageView.backgroundColor = UIColor.redColor()
                
                let backgroundView = UIView(forAutoLayout: ())
                backgroundView.backgroundColor = UIColor.blackColor()
                backgroundView.alpha = 0.6
                
                imageView.addSubview(backgroundView)
                
                backgroundView.autoPinEdgesToSuperviewEdges()
                
                let textLabel = UILabel(forAutoLayout: ())
                textLabel.text = "Chatea con tú ciudad"
                textLabel.textColor = UIColor.whiteColor()
                textLabel.textAlignment = .Center
                textLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
                textLabel.numberOfLines = 0
                
                imageView.addSubview(textLabel)
                
                textLabel.autoAlignAxis(.Vertical, toSameAxisOfView: imageView, withOffset: 0)
                textLabel.autoAlignAxis(.Horizontal, toSameAxisOfView: imageView, withOffset: 100)
                textLabel.autoConstrainAttribute(.Width, toAttribute: .Width, ofView: imageView, withMultiplier: 0.8, relation: .LessThanOrEqual)
                
                scrollView.addSubview(imageView)
                
                imageView.autoPinEdge(.Left, toEdge: .Left, ofView: scrollView)
                imageView.autoPinEdge(.Top, toEdge: .Top, ofView: scrollView)
                imageView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: scrollView)
                imageView.autoSetDimensionsToSize(CGSize(width: view.bounds.width, height: view.bounds.height))
                
                previousImageView = imageView
            } else if index == 2 {
                let imageView = UIImageView(forAutoLayout: ())
                imageView.image = UIImage(named: "walkthrough3")
                imageView.backgroundColor = UIColor.blueColor()
                
                let backgroundView = UIView(forAutoLayout: ())
                backgroundView.backgroundColor = UIColor.blackColor()
                backgroundView.alpha = 0.6
                
                imageView.addSubview(backgroundView)
                
                backgroundView.autoPinEdgesToSuperviewEdges()
                
                let textLabel = UILabel(forAutoLayout: ())
                textLabel.text = "Todo sobre tú ciudad en un sólo lugar"
                textLabel.textColor = UIColor.whiteColor()
                textLabel.textAlignment = .Center
                textLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
                textLabel.numberOfLines = 0
                
                imageView.addSubview(textLabel)
                
                textLabel.autoAlignAxis(.Vertical, toSameAxisOfView: imageView, withOffset: 0)
                textLabel.autoAlignAxis(.Horizontal, toSameAxisOfView: imageView, withOffset: 100)
                textLabel.autoConstrainAttribute(.Width, toAttribute: .Width, ofView: imageView, withMultiplier: 0.8, relation: .LessThanOrEqual)
                
                scrollView.addSubview(imageView)
                
                imageView.autoPinEdge(.Left, toEdge: .Right, ofView: previousImageView!)
                imageView.autoPinEdge(.Right, toEdge: .Right, ofView: scrollView)
                imageView.autoPinEdge(.Top, toEdge: .Top, ofView: scrollView)
                imageView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: scrollView)
                imageView.autoSetDimensionsToSize(CGSize(width: view.bounds.width, height: view.bounds.height))
                
                previousImageView = imageView
            } else {
                let imageView = UIImageView(forAutoLayout: ())
                imageView.image = UIImage(named: "walkthrough2")
                imageView.backgroundColor = UIColor.grayColor()
                
                let backgroundView = UIView(forAutoLayout: ())
                backgroundView.backgroundColor = UIColor.blackColor()
                backgroundView.alpha = 0.6
                
                imageView.addSubview(backgroundView)
                
                backgroundView.autoPinEdgesToSuperviewEdges()
                
                let textLabel = UILabel(forAutoLayout: ())
                textLabel.text = "Pregunta tramites gubernamentales o servicios públicos"
                textLabel.textColor = UIColor.whiteColor()
                textLabel.textAlignment = .Center
                textLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
                textLabel.numberOfLines = 0
                
                imageView.addSubview(textLabel)
                
                textLabel.autoAlignAxis(.Vertical, toSameAxisOfView: imageView, withOffset: 0)
                textLabel.autoAlignAxis(.Horizontal, toSameAxisOfView: imageView, withOffset: 100)
                textLabel.autoConstrainAttribute(.Width, toAttribute: .Width, ofView: imageView, withMultiplier: 0.8, relation: .LessThanOrEqual)
                
                scrollView.addSubview(imageView)
                
                imageView.autoPinEdge(.Left, toEdge: .Right, ofView: previousImageView!)
                imageView.autoPinEdge(.Top, toEdge: .Top, ofView: scrollView)
                imageView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: scrollView)
                imageView.autoSetDimensionsToSize(CGSize(width: view.bounds.width, height: view.bounds.height))
                
                previousImageView = imageView
            }
        }
    }
    
    // MARK: Page action
    
    @IBAction func changePage(sender: AnyObject) {
        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPointMake(x, 0), animated: true)
    }
    
    // MARK: Scrollview delegate
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width);
        pageControl.currentPage = Int(pageNumber)
    }
    

    @IBAction func enterPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
