//  Copyright © 2016 Arpad Larrinaga Aviles
//
//  City.mx is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  City.mx is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with City.mx.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
import Chatto
import ChattoAdditions

class TextMessageHandler: BaseMessageInteractionHandlerProtocol {
    private let baseHandler: BaseMessageHandler
    init (baseHandler: BaseMessageHandler) {
        self.baseHandler = baseHandler
    }
    func userDidTapOnFailIcon(viewModel viewModel: TextMessageViewModel) {
        self.baseHandler.userDidTapOnFailIcon(viewModel: viewModel)
    }

    func userDidTapOnBubble(viewModel viewModel: TextMessageViewModel) {
        self.baseHandler.userDidTapOnBubble(viewModel: viewModel)
    }

    func userDidLongPressOnBubble(viewModel viewModel: TextMessageViewModel) {
        self.baseHandler.userDidLongPressOnBubble(viewModel: viewModel)
    }
}

class PhotoMessageHandler: BaseMessageInteractionHandlerProtocol {
    private let baseHandler: BaseMessageHandler
    init (baseHandler: BaseMessageHandler) {
        self.baseHandler = baseHandler
    }

    func userDidTapOnFailIcon(viewModel viewModel: PhotoMessageViewModel) {
        self.baseHandler.userDidTapOnFailIcon(viewModel: viewModel)
    }

    func userDidTapOnBubble(viewModel viewModel: PhotoMessageViewModel) {
        self.baseHandler.userDidTapOnBubble(viewModel: viewModel)
    }

    func userDidLongPressOnBubble(viewModel viewModel: PhotoMessageViewModel) {
        self.baseHandler.userDidLongPressOnBubble(viewModel: viewModel)
    }
}

class BaseMessageHandler {

    private let messageSender: FakeMessageSender
    init (messageSender: FakeMessageSender) {
        self.messageSender = messageSender
    }
    func userDidTapOnFailIcon(viewModel viewModel: MessageViewModelProtocol) {
        NSLog("userDidTapOnFailIcon")
        self.messageSender.sendMessage(viewModel.messageModel)
    }

    func userDidTapOnBubble(viewModel viewModel: MessageViewModelProtocol) {
        NSLog("userDidTapOnBubble")

    }

    func userDidLongPressOnBubble(viewModel viewModel: MessageViewModelProtocol) {
        NSLog("userDidLongPressOnBubble")
    }
}
