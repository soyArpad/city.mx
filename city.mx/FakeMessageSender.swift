//  Copyright © 2016 Arpad Larrinaga Aviles
//
//  City.mx is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  City.mx is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with City.mx.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
import Chatto
import ChattoAdditions

public class FakeMessageSender {

    public var onMessageChanged: ((message: MessageModelProtocol) -> Void)?

    public func sendMessages(messages: [MessageModelProtocol]) {
        for message in messages {
            self.fakeMessageStatus(message)
        }
    }

    public func sendMessage(message: MessageModelProtocol) {
        self.fakeMessageStatus(message)
    }

    private func fakeMessageStatus(message: MessageModelProtocol) {
        switch message.status {
        case .Success:
            break
        case .Failed:
            self.updateMessage(message, status: .Success)
            break
        case .Sending:
            self.updateMessage(message, status: .Success)
            break
        }
    }

    private func updateMessage(message: MessageModelProtocol, status: MessageStatus) {
        if message.status != status {
            message.status = status
            self.notifyMessageChanged(message)
        }
    }

    private func notifyMessageChanged(message: MessageModelProtocol) {
        self.onMessageChanged?(message: message)
    }
}
