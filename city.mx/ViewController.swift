//  Copyright © 2016 Arpad Larrinaga Aviles
//
//  City.mx is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  City.mx is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//    
//  You should have received a copy of the GNU General Public License
//  along with City.mx.  If not, see <http://www.gnu.org/licenses/>.

import UIKit
import NVActivityIndicatorView

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    
    var kbHeight: CGFloat!
    var kbZero: CGFloat! = 0
    let limitLength = 25
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "City MX"
        searchTextField.delegate = self
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 10, y: 10, width: 100, height: 100), type: .BallScaleMultiple, color: UIColor.purpleColor(), size: CGSize(width: 100, height: 100))
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimation()
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func tapBackground(sender: AnyObject) {
        searchTextField.resignFirstResponder()
    }
    
    @IBAction func enterPressed(sender: AnyObject) {
        //nameTextField.resignFirstResponder()
//        //self.animateTextField(false)
//        
//        if nameTextField.text!.characters.count <= 0 {
//            let alertController = UIAlertController(title: "Por favor ingresa tu nombre para que los festejados puedan saber quien eres", message: nil, preferredStyle: .Alert)
//            
//            let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
//            alertController.addAction(okAction)
//            
//            self.presentViewController(alertController, animated: true, completion: nil)
//        }
//        else {
//            var tokenString = "TFM|" + nameTextField.text!.uppercaseString + "||" + UIDevice.currentDevice().identifierForVendor!.UUIDString + ""
//            
//            let password = "iWATFMkey$99"
//            
//            tokenString = AESCrypt.encrypt(tokenString, password: password)
//            
//            let userdefaults = NSUserDefaults.standardUserDefaults()
//            userdefaults.setObject(tokenString, forKey: "TOKENKEY")
//            userdefaults.setObject(nameTextField.text!.uppercaseString, forKey: "NAMEKEY")
//            userdefaults.synchronize()
//            
//            self.dismissViewControllerAnimated(true, completion: nil)
//            
//            NSNotificationCenter.defaultCenter().postNotificationName("USER_LOGGED_NOTIFICATION", object: nil)
//        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        enterPressed(textField)
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= limitLength
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                kbHeight = keyboardSize.height
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    func animateTextField(up: Bool) {
        guard let movement = (up ? -kbHeight : kbZero) else {
            return
        }
        bottomLayoutConstraint.constant = 0 + movement;
        
        UIView.animateWithDuration(0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

